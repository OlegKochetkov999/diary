<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\DataTransformer\StringToDateTimeTransformer;
use AppBundle\Entity\Item\Item as EntityItem;
use AppBundle\Entity\Item\Category;

class Item extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => 'Наименование'])
            ->add('total', TextType::class, [
                'label' => 'Сумма',
                'required' => true
            ])
            ->add('type', ChoiceType::class, [
                'choices' => array_flip(EntityItem::$types),
                'label' => 'Тип',
                'required' => true
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'label' => 'Категория',
                'required' => false
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Примечание',
                'required' => false,
                'attr' => ['class' => 'form-textarea']
            ])
            ->add('date', HiddenType::class, [
                'label' => false,
            ]);
        
        $builder->get('date')->addModelTransformer(new StringToDateTimeTransformer());
    }
    
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'item';
    }
}
