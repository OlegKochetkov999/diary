<?php

namespace AppBundle\Form\Type\Analytics;

use AppBundle\Utils\Date;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AnalyticsForMonth extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('month', ChoiceType::class, [
                'label' => 'Месяц',
                'choices' => Date::$months])
            ->add('year', ChoiceType::class, [
                'label' => 'Год',
                'choices' => [2017 => 2017, 2018 => 2018]
            ])
        ;
    }
    
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'for_month';
    }
}
