<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use AppBundle\Entity\User\Permission;

class Role extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, ['label' => 'Наименование'])
                ->add('permissions', ChoiceType::class, [
                    'label' => false, 
                    'expanded' => true, 
                    'choices' => Permission::getRoles(), 
                    'multiple' => true
                ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'role';
    }
}
