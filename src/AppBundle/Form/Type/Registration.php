<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityManager;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use AppBundle\Entity\User\Role;

class Registration extends AbstractType
{
    /**
     * @var EntityManager $em
     */
    protected $em;
    
    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, ['label' => 'Имя пользователя'])
            ->add('fullname', TextType::class, ['label' => 'ФИО'])
            ->add('email', TextType::class, [
                'label' => 'Электронная почта',
                'required' => false
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Пароль'
            ])
            ->add('dateOfBirth', TextType::class, [
                'label' => 'Дата рождения',
                'required' => false
            ])
            ->add('captcha', CaptchaType::class, [
                'invalid_message' => 'Введен неверный код'
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function(FormEvent $event){
                if ($data = $event->getData()) {
                    if ($form = $event->getForm() and $user = $form->getData()) {
                        $user->setDateOfRegistration(new \DateTime("now"));
                        $role = $this->em->getRepository(Role::class)->findOneByCode("ROLE_USER");
                        if ($role) {
                            $user->setRole($role);
                            $form->setData($user);
                        }
                    }
                }
            });
    }
    
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'registration';
    }
}
