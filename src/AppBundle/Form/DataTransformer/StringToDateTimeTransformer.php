<?php
namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class StringToDateTimeTransformer implements DataTransformerInterface
{
    /**
     * @param \DateTime $date
     * @return String
     */
    public function transform($date)
    {
        return $date ? $date->format("d-m-Y") : "";
    }
    
    /**
     * @param string $stringDate
     * @return \DateTime
     */
    public function reverseTransform($stringDate)
    {
        return new \DateTime($stringDate);
    }
}
