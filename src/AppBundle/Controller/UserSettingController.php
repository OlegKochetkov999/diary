<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/user-setting")
 */
class UserSettingController extends AbstractCRUDController
{
    protected function getEntityCatalog()
    {
        return 'User\\';
    }
    
    public function getEntityName()
    {
        return "UserSetting";
    }
    
    public function getEntityTitle()
    {
        return "Настройки пользователя";
    }
}
