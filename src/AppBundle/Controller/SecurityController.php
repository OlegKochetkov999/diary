<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use AppBundle\Entity\User\User;
use AppBundle\Form\Type\Registration;

/**
 * @Route("/")
 */
class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="security_login")
     */
    public function loginAction()
    {
        $helper = $this->get('security.authentication_utils');
        $form = $this->createForm(Registration::class, new User());
        
        return $this->render('AppBundle:Security:login.html.twig', [
            'form' => $form->createView(),
            'last_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction()
    {
    }
    
    /**
     * @param Request $request
     * @Route("/registration", name="app_security_registration")
     */
    public function registrationAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(Registration::class, $user);

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                // try {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    
                    $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                    $this->get('security.token_storage')->setToken($token);
                    $this->get('session')->set('_security_main',serialize($token));
                    return $this->redirect($this->generateUrl('main_index'));
              //  } catch (\InvalidArgumentException $e) {
              //      $this->handlingFormError($form, $e);
              //  }
            }
        }
        
        return $this->render('AppBundle:Security:login.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
