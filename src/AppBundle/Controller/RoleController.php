<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/role")
 */
class RoleController extends AbstractCRUDController
{
    protected function getEntityCatalog()
    {
        return 'User\\';
    }
    
    protected function getEntityName()
    {
        return "Role";
    }

    protected function getEntityTitle()
    {
        return "Роль";
    }
}
