<?php

namespace AppBundle\Controller;

use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

abstract class AbstractController extends Controller
{
    protected function handlingFormError($form, $e)
    { 
    }
    
    protected function getManager()
    {
        return $this->getDoctrine()->getManager();
    }
    
    /**
     * @param QueryBuilder $builder
     * @return QueryBuilder
     */
    protected function getPermissionBuilder($builder)
    {
        return $builder->andWhere("e.user = {$this->getUser()}");
    }

    protected function userConformance($id)
    {
        return ($this->getUser() and $this->getUser() == $id);
    }
    
    /**
     * Check permissions option for accessing on some page
     *
     * @param string $path
     * @param string $type
     * @throws AccessDeniedException
     */
    protected function checkPermission($path, $type = null)
    {
        $role = "ROLE" . str_replace("/", "_", strtoupper($path)) . $type;

        /* @var User $user */
        $user = $this->getUser();
        if ($user) {
            /*if ($user->isActive()) {
                $ip = $_SERVER['REMOTE_ADDR'];
            } else {
                $user->setLastActivity(new \DateTime());
                $this->getDoctrine()->getEntityManager()->flush($user);
            }*/

            if ($userRole = $user->getRole()) {
                $permissions = $userRole->getPermissions();
                if ($permissions and in_array($role, $permissions)) {
                    return null;
                }
            } else {
                return $this->render('AppBundle:Error:error.html.twig', ["message" => "Обратитесь к администратору системы для получения пользовательской роли"]);
            }
        } else {
            return $this->redirectToRoute('login');
        }

        return $this->render('AppBundle:Error:error.html.twig', ["message" => "Для просмотра этой страницы у Вас не достаточно прав доступа"]);
    }
}
