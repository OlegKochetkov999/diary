<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Utils\Response AS CustomResponse;

/**
 * @Route("/notification")
 */
class NotificationController extends Controller
{
    /**
     * @param Request $request
     * @Route("/active", name="api_notification_active")
     */
    public function getActiveAction(Request $request)
    {
        if ($user = $this->getUser()) {
            $result = '';
            $id = $user->getId();
            
            if ($cache = $this->memcached()->get($id)) {
                $result = $cache;
                $this->memcached()->delete($id);
            }
            
            return $this->json(CustomResponse::generate(200, $result));
        }
        
        return $this->json(CustomResponse::generate(401));
    }
    
    protected function memcached()
    {
        return $this->get('app.cache.memcached');
    }
}
