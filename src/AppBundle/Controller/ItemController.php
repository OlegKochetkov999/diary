<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/item")
 */
class ItemController extends AbstractCRUDController
{
    protected function getEntityCatalog()
    {
        return 'Item\\';
    }
    
    protected function getEntityName()
    {
        return "Item";
    }

    protected function getEntityTitle()
    {
        return "Статьи";
    }
}
