<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Item\Item;
use AppBundle\Form\Type\Analytics\AnalyticsForMonth;

/**
 * @Route("/analytics")
 */
class AnalyticsController extends AbstractController
{
    /**
     * @param Request $request
     * @Route("/", name="analytics_index")
     */
    public function indexAction(Request $request)
    {
        if ($user = $this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            $formCreateItem = $this->createForm(AnalyticsForMonth::class);
            
            return $this->render('AppBundle:Analytics:index.html.twig', [
                        'formAnalytics' => $formCreateItem->createView()
                    ]);
        }
        
        return $this->redirectToRoute('app_security_registration');
    }
    
    /**
     * @param Request $request
     * @Route("/type", name="analytics_type")
     */
    public function analyticsAction(Request $request)
    {
        if ($user = $this->getUser()) {
            
        }
        
        return $this->redirectToRoute('app_security_registration');
    }
}
