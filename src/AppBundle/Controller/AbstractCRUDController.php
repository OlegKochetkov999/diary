<?php

namespace AppBundle\Controller;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityRepository;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/*
 * Немного магии
 *
 * Для создания страниц стандартных действий для сущности необходимо определить метод
 * getEntityName()
 * который возвращает название класса сущности (напр. 'Entity')
 * Форма должна называться EntityType, а все шаблоны находиться в папке
 * Resources/views/Entity
 * Названия шаблонов list, add, edit, view (.html.twig)
 *
 * Если репозиторий не совпадает с EntityRepository - переопределить getRepository
 * Нужны особые формы для создания и редактирования - переопределить getFormAddTypeName и getFormEditTypeName
 * Ну а также можно переопределить одно или несколько действий целиком.
 */

abstract class AbstractCRUDController extends AbstractController
{
    /**
     * Get list entities
     * url - "entity/", route - "app_entity_list"
     *
     * @param Request $request
     * @return array
     *
     * @Route("/")
     */
    public function listAction(Request $request)
    {
        if ($permission = $this->checkPermission($request->getPathInfo(), "LIST")) {
            return $permission;
        }
        
        $filterBuilder = $this->getQueryBuilder();
        $filter = $this->createForm($this->getFilterName());

        if ($request->query->has($filter->getName())) {
            $filter->submit($request->query->get($filter->getName()));
        }

        $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filter, $filterBuilder);

        $entities = $this->getPaginator()->paginate($filterBuilder->getQuery(), $request->query->getInt('page', 1), $request->query->getInt('limit', 17));

        return $this->render("AppBundle:" . $this->getEntityName() . ":list.html.twig", [
            'entities' => $entities,
            'filter' => $filter->createView(),
            'entityName' => $this->getEntityName()
        ]);
    }

    /**
     * Create entity
     * url - "entity/add", route - "app_entity_add"
     *
     * @param Request $request
     * @return array
     *
     * @Route("/add")
     */
    public function addAction(Request $request)
    {
        if ($permission = $this->checkPermission($request->getPathInfo())) {
      //      return $permission;
        }

        $class = $this->getEntityClassName();
        $entity = new $class();
        $form = $this->createForm($this->getFormAddTypeName(), $entity);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($entity);
                    $em->flush();
                    return $this->redirect($this->generateUrl('app_' . strtolower($this->getEntityName()) . '_view', array('id' => $entity->getId())));
                } catch (\InvalidArgumentException $e) {
                    $this->handlingFormError($form, $e);
                }
            }
        }

        return $this->render("AppBundle:" . $this->getEntityName() . ":" . lcfirst($this->getEntityName()) . ".form.html.twig", [
            'entity' => $entity,
            'form' => $form->createView(),
            'entityName' => $this->getEntityName()
        ]);
    }

    /**
     * View page for entity
     * url - "entity/view/id", route - "app_entity_view"
     *
     * @Route("/view/{id}")
     */
    public function viewAction(Request $request, $id)
    {
        $path = $request->getPathInfo();
        if ($permission = $this->checkPermission(substr($path, 0, strripos($path, "/")))) {
            return $permission;
        }

        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw $this->createNotFoundException("${$this->getEntityTitle()} с номером $id отсутствует");
        }

        return $this->render("AppBundle:" . $this->getEntityName() . ":view.html.twig", [
            'entity' => $entity,
            'entityName' => $this->getEntityName(),
        ]);
    }

    /**
     * Updating entity
     * url - "entity/edit/id", route - "app_entity_edit"
     *
     * @Route("/edit/{id}")
     */
    public function editAction(Request $request, $id)
    {
        $path = $request->getPathInfo();
        if ($permission = $this->checkPermission(substr($path, 0, strripos($path, "/")))) {
            return $permission;
        }

        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw $this->createNotFoundException("${$this->getEntityTitle()} с номером $id отсутствует");
        }

        $form = $this->createForm($this->getFormEditTypeName(), $entity);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                    return $this->redirect($this->generateUrl('app_' . strtolower($this->getEntityName()) . '_view', ['id' => $entity->getId()]));
                } catch (\InvalidArgumentException $e) {
                    $this->handlingFormError($form, $e);
                }
            }
        }

        return $this->render("AppBundle:" . $this->getEntityName() . ":" . lcfirst($this->getEntityName()) . ".form.html.twig",[
            'entity' => $entity,
            'form' => $form->createView(),
            'entityName' => $this->getEntityName()
        ]);
    }

    /**
     * @Route("/delete/{id}")
     */
    public function deleteAction(Request $request, $id)
    {
        $path = $request->getPathInfo();
        if ($permission = $this->checkPermission(substr($path, 0, strripos($path, "/")))) {
            return $permission;
        }

        $entity = $this->getRepository()->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return $this->redirectToRoute('app_' . strtolower($this->getEntityName()) . '_list');
    }

    protected function getEntityCatalog()
    {
        return '';
    }
    
    /**
     * Return a full path to Entity class
     *
     * @return string
     */
    protected function getEntityClassName()
    {
        return "AppBundle\\Entity\\" . $this->getEntityCatalog() . $this->getEntityName();
    }

    /**
     * Return a full path to add Form class
     *
     * @return string
     */
    protected function getFormAddTypeName()
    {
        return "AppBundle\\Form\\Type\\" . $this->getEntityName();
    }

    /**
     * Return a full path to edit Form class
     *
     * @return string
     */
    protected function getFormEditTypeName()
    {
        return "AppBundle\\Form\\Type\\" . $this->getEntityName();
    }

    /**
     * Return a full path to filter Form class
     *
     * @return string
     */
    protected function getFilterName()
    {
    return "AppBundle\\Form\\Type\\Filter\\" . $this->getEntityName();
    }

    /**
     * Get repository of Entity
     *
     * @return EntityRepository
     */
    protected function getRepository()
    {
        return $this->getDoctrine()
                    ->getManager()
                    ->getRepository("AppBundle:" . $this->getEntityCatalog() . $this->getEntityName());
    }

    /**
     * Возвращает объект для получения списка сущностей
     *
     * @return QueryBuilder
     */
    protected function getQueryBuilder()
    {
        $repository = $this->getRepository();
        if ($repository instanceof FilterRepositoryInterface) {
            return $repository->createFilterBuilder();
        } else {
            return $repository->createQueryBuilder('e');
        }
    }

    /**
     * @return Paginator
     */
    protected function getPaginator()
    {
        return $this->get('knp_paginator');
    }

    /**
     * Get Entity name
     *
     * @return string
     */
    abstract protected function getEntityName();
    
    /**
     * Get Entity title on russian language
     *
     * @return string
     */
    abstract protected function getEntityTitle();
}
