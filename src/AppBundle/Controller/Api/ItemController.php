<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Utils\Response AS CustomResponse;

/**
 * @Route("/item")
 */
class ItemController extends AbstractApiController
{
    protected function getEntityCatalog()
    {
        return 'Item\\';
    }
    
    protected function getEntityName()
    {
        return "Item";
    }

    protected function getEntityTitle()
    {
        return "Статья";
    }
    
    /**
     * @param Request $request
     * @Route("/by-date", name="api_item_by_date")
     */
    public function getByDateAction(Request $request)
    {
        if ($date = $request->get("date")) {
            $condition = ["date" => new \DateTime($date), "user" => $this->getUser()];
            $entities = $this->getRepository()->findBy($condition);
            $result = $this->serialize($entities);
            return $this->json(CustomResponse::generate(200, $result));
        }
        
        return $this->json(CustomResponse::generate(400));
    }
}
