<?php

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use AppBundle\Entity\UserAttribute;
use AppBundle\Controller\AbstractCRUDController;
use AppBundle\Normalizer\DateTimeNormalizer;
use AppBundle\Normalizer\IdentityNormalizer;
use AppBundle\Utils\Response AS CustomResponse;
use AppBundle\Entity\User\User;

abstract class AbstractApiController extends AbstractCRUDController
{
    protected $serializer;
    
    public function __construct()
    {
        $this->serializer = new Serializer([new DateTimeNormalizer('d-m-Y'), new GetSetMethodNormalizer(), new IdentityNormalizer()], [new JsonEncoder()]);
    }
    
    /**
     * @param mixed $objects
     * @param string $type
     * @return string
     */
    protected function serialize($objects, $type = 'json')
    {
        return $this->serializer->serialize($objects, $type);
    }
    
    /**
     * Get list entities
     * url - "api/entity/", route - "api_entity_list"
     *
     * @param Request $request
     * @return array
     *
     * @Route("/")
     */
    public function listAction(Request $request)
    {
        if ($permission = $this->checkPermission($request->getPathInfo(), "LIST")) {
            return $permission;
        }
        
        $entities = $this->getRepository($this->getEntityClassName())->findByUser($this->getUser());

        return $this->serializer->serialize($entities, 'json');
    }

    /**
     * Create entity
     * url - "entity/add", route - "app_entity_add"
     *
     * @param Request $request
     * @return array
     *
     * @Route("/add")
     */
    public function addAction(Request $request)
    {
        if ($permission = $this->checkPermission($request->getPathInfo())) {
            return $permission;
        }

        $class = $this->getEntityClassName();
        $entity = new $class();
        $form = $this->createForm($this->getFormAddTypeName(), $entity); //TODO: replace on validator

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                try {
                    $em = $this->getDoctrine()->getManager();
                    
                    if ($entity instanceof UserAttribute) {
                        $entity->setUser($this->getUser());
                    }
                    
                    $em->persist($entity);
                    $em->flush();
                    $entity = $this->serializer->serialize($entity, 'json');
                    return $this->json(CustomResponse::generate(200, $entity));
                } catch (\InvalidArgumentException $e) {
                    return $this->json(CustomResponse::generate(500, $e->getMessage()));
                }
            } else {
                return $this->json(CustomResponse::generate(500, $this->getErrorMessages($form)));
            }
        }

        return $this->json(CustomResponse::generate(400));
    }

    /**
     * Updating entity
     * url - "entity/edit/id", route - "app_entity_edit"
     *
     * @Route("/edit/{id}")
     */
    public function editAction(Request $request, $id)
    {
        $path = $request->getPathInfo();
        if ($permission = $this->checkPermission(substr($path, 0, strripos($path, "/")))) {
            return $permission;
        }

        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw $this->createNotFoundException("${$this->getEntityTitle()} с номером $id отсутствует");
        }

        $form = $this->createForm($this->getFormEditTypeName(), $entity);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                try {
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();

                } catch (\InvalidArgumentException $e) {
                    $this->handlingFormError($form, $e);
                }
            }
        }

        return $this->json(CustomResponse::generate(200));
    }

    /**
     * @Route("/delete/{id}")
     */
    public function deleteAction(Request $request, $id)
    {
        $path = $request->getPathInfo();
        if ($permission = $this->checkPermission(substr($path, 0, strripos($path, "/")))) {
            return $permission;
        }

        $entity = $this->getRepository()->find($id);
        
        if ($entity instanceof UserAttribute) {
            if (!$entity->isOwner($this->getUser())) {
                return $this->json(CustomResponse::generate(403));
            }
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return $this->json(CustomResponse::generate(200));
    }

    
    protected function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = [];

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
    
    /**
     * Check permissions option for accessing on some page
     *
     * @param string $path
     * @param string $type
     * @throws AccessDeniedException
     */
    protected function checkPermission($path, $type = null)
    {
        return null; //TODO fix this role hack after create role redact page
        $permission = "ROLE" . str_replace("/", "_", strtoupper($path)) . $type;

        /** @var User $user */
        $user = $this->getUser();
        
        if ($user) {
            $role = $user->getRole();
            $permissions = $role->getPermissions();
            if ($role and $permissions and in_array($permission, $permissions)) {
                return null;
            }
        } else {
            return $this->json(CustomResponse::generate(401));
        }

        return $this->json(CustomResponse::generate(403));
    }
}
