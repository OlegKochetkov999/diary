<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Item\Item;
use AppBundle\Form\Type\Item as FormItem;

/**
 * @Route("/main")
 */
class MainController extends AbstractController
{
    /**
     * @param Request $request
     * @Route("/", name="main_index")
     */
    public function indexAction(Request $request)
    {
        if ($user = $this->getUser()) {
            $em = $this->getDoctrine()->getManager();
            $formCreateItem = $this->createForm(FormItem::class, new Item($user));
            $date = new \DateTime();
            $date->setTime(0, 0, 0);
            $items = $em->getRepository(Item::class)->findBy(["date" => $date, "user" => $this->getUser()]);
            
            return $this->render('AppBundle:Main:index.html.twig', [
                        'items' => $items,
                        'formCreateItem' => $formCreateItem->createView()
                    ]);
        }
        
        return $this->redirectToRoute('app_security_registration');
    }
}
