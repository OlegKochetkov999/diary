class Calendar {
    get days(){ return this._days;}
    
    constructor(element){
        this._element = element;
        
        let now = new Date();
        this._nowDay = now.getDate();
        this._nowMonth = now.getMonth();
        this._nowYear = now.getYear() + 1900;
        
        this._countCells = 42;
        this._countDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        this._months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
        this._days = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];
    }
    
    getCountDaysInMonth(month, year) {
        return (month == 1 && year % 4 == 0 && ((year % 100 != 0) || (year % 400 == 0)))  ? 29 : this._countDays[month];
    }
    
    /**
     * Create calendar widget and adding it in custom element
     * @param {Object} obj <p>{month, year}</p>
     */
    create(obj){
        if (typeof obj != "Array" &&
            typeof obj != "undefined" &&   
            (obj.month || obj.month == 0) 
            && obj.year) {
            this._element.html(this.getTemplate(parseInt(obj.month), parseInt(obj.year)));
        } else {
            this._element.html(this.getTemplate(this._nowMonth, this._nowYear));
        }
    }
    
    /**
     * Return object with set data about next month
     * @param {Number} month
     * @param {Number} year
     * @returns {Object}
     */
    getDateNext(month, year){
        let nextMonth = (month == 11) ? 0 : month + 1;
        let nextYear = (nextMonth == 0) ? year + 1 : year;
        let title = `${this._months[nextMonth]} ${nextYear}`;
        return {
            "month": nextMonth, 
            "year": nextYear, 
            "title": title
        };
    }
    
    /**
     * Return object with set data about previous month
     * @param {Number} month
     * @param {Number} year
     * @returns {Object} <p>{month: Number, year: Number, title: String}</p>
     */
    getDatePrev(month, year){
        let prevMonth = (month == 0) ? 11 : month - 1;
        let prevYear = (prevMonth == 11) ? year - 1 : year;
        let title = `${this._months[prevMonth]} ${prevYear}`;
        return {
            "month": prevMonth, 
            "year": prevYear, 
            "title": title
        };
    }
    
    /**
     * Get array objects days
     * @returns {Array}
     */
    getArrayDays(month, year){
        let firstDayDate = new Date(year, month, 1);
        let firstDay = firstDayDate.getDay() != 0 ? firstDayDate.getDay() : 7;
    
        let data = this.getDatePrev(month, year);
    
        let prevCountDays = this.getCountDaysInMonth(data.month, data.year);
        let cells = [];
        let cellMonth = month + 1;
    
        for (let day = 1, nextMonthDay = 1; day < this._countCells; day++) {
            let cellDay = day - firstDay + 1;
            let isActive = (this._nowDay == cellDay && this._nowMonth == month && this._nowYear == year);
            
            let cell = {
                "day": cellDay,
                "month": cellMonth,
                "year": year,
                "classMonth": "current-month",
                "classDay": `calendar-day-${cellDay}`,
                "activeClass": isActive ? "current-month--active" : ""
            };
        
            if (day < firstDay) { // previous month days
                cell.day = prevCountDays + cellDay;
                cell.classMonth = "other-month";
            } else if (day >= firstDay + this.getCountDaysInMonth(month, year)) { //next month days
                cell.day = nextMonthDay++;
                cell.classMonth = "other-month";
                cell.classDay = "";
            }
        
            cells.push(cell);
        }
        
        return cells;
    }
    
    /**
     * Get title month
     * @param {Number} month
     * @param {Year} year
     * @param {Boolean} isPrev
     * @returns {String}
     */
    getMonthTitle(month, year, isPrev){
        let data = isPrev ? this.getDatePrev(month, year) : this.getDateNext(month, year);
        return `<span class="close-month" data-month="${data.month}" data-year="${data.year}" title="${data.title}">${data.title}</span>`;
    }
    
    /**
     * Get template cell of matrix days
     * @param {Object} data <p>{classMonth: String, classDay: String, day: Number}</p>
     * @returns {String}
     */
    getCellTemplate(data) {
        let classes = `${data.classMonth} ${data.classDay} ${data.activeClass}`;
        let date = `${data.day}-${data.month}-${data.year}`;
        return `<div class="${classes} block-table-cell" data-date="${date}">
                    <span class="day">${data.day}</span>
                </div>`;
    }
    
    /**
     * Get string template from array cells content
     * @param {Array} cells
     * @returns {String}
     */
    getTemplateArrayCells(cells){
        let count = 0;
        let content = [];
        let buffer = '';
        let that = this;
        
        cells.forEach(function(item, i){
            count++;
            buffer += that.getCellTemplate(item);
            if (count == 7) {
                count = 0;
                content.push(`<div class="block-week-${i} block-table-row">${buffer}</div>`);
                buffer = '';
            }
        });
        
        return content.join('');
    }
    
    /**
     * Get string template titles of days week
     * @returns {String}
     */
    getTemplateDaysTitle(){
        let daysTitle = '';
    
        this._days.forEach(function(item, i) {
            daysTitle += `<div class="weekday-title block-table-head">${item}</div>`;
        });
        
        return daysTitle;
    }
    
    /**
     * Create template calendar table
     * @param {Number} month
     * @param {Number} year
     * @returns {String}
     */
    getTemplate(month, year) {
        return `<div class="calendar-nav">
                    <div class="month-prev">${this.getMonthTitle(month, year, true)}</div>
                    <div class="month-current">${this._months[month]} ${year}</div>
                    <div class="month-next">${this.getMonthTitle(month, year)}</div>
                </div>
                <div class="calendar-month block-table" id="calendar-month-${month}">	
                    <div class="block-table-heading">${this.getTemplateDaysTitle()}</div>
                    ${this.getTemplateArrayCells(this.getArrayDays(month, year))}
                </div>`;
    }
}
