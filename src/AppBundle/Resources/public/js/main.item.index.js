let appIndex;

$.fn.extend({
    animateCss: function (animationName, callback) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
            
            if (typeof callback != "undefined") {
                callback();
            }
        });
        return this;
    }
});

$(function () {
    appIndex = new MainIndex();
});

function MainIndex(){
    let items = [];
    let ss = new SelectorStorage();
    let calendar = new Calendar($("#calendar"));
    let cellSelector = ".block-table-cell.current-month";
    let cellActiveClass = "current-month--active";
    let weekDaySelector = ".weekday-title.block-table-head";
    
    let itemTemplateClass = "item-template";
    let itemList = "#item-list";
    
    let itemCreateFormSelector = "#form-item-create";
    let itemCreateFormModalSelector = "#form-item-create-modal";
    let itemCreateButton = $("#item-create");
    
    let itemViewModalSelector = "#view-item-modal";
    let itemViewModalButtonSelector = ".icon.icon-question";
    
    function toggleModal(selector) {
        let modal = $(selector);
        
        if (modal.hasClass(ss.hide)) {
            modal.animateCss("fadeIn").removeClass(ss.hide);
        } else {
            modal.animateCss("fadeOut", function(){
                modal.addClass(ss.hide);
            });
        }
    };
    
    /**
     * Get String date by current active date block in calendar
     * @returns {String}
     */
    function getCurrentDate(){
        return $(`.${cellActiveClass}`).data("date");
    };
    
    /**
     * Update date field in item form
     */
    function updateDateItem(){
        $("#item_date").val(getCurrentDate());
    };
    
    /**
     * Clear all about old items
     */
    function clearItems() {
        let itemCards = $(itemList).find("[id^=item-card-]");
        itemCards.animateCss('flipOutX', function(){
            itemCards.remove();
        });
        items = [];
    };
    
    function initItemViewModal(id){
        let item = findItem(id);
        let $fields = $(`${itemViewModalSelector} div.text-span`);
        $.each($fields, function(key, field){
            field.innerHTML = item[field.dataset.field];
        });
        
        toggleModal(itemViewModalSelector);
    };
    
    /**
     * Find item data by id
     * @param {Number} id
     * @returns {Object}
     */
    function findItem(id) {
        let item = null;
        $.each(items, function(key, value){
            if (value.id == id) {
                item = value.data;
            }
        });
        
        return item;
    };
    
    /**
     * Find card item in html by id
     * @param {Number} id
     * @returns {Object}
     */
    function findItemCard(id) {
        let item = null;
        let items = $("div[id^=item-card-]");
        if (items.length > 0) {
            $.each(items, function(key, block){
                if (block.dataset.id == id) {
                    item = block;
                }
            });
        }
        
        return item;
    };
    
    /**
     * Remove some icons actions fro card by item card object
     * @param {Object} card
     */
    function removeIcons(card){
        $(card).find(".icon-action.icon-close").remove();
        $(card).find(".icon-action.icon-edit").remove();
        $(card).find(".icon-action.icon-done").remove();
    };
    
    /**
     * Creating item card from template and data, and inserting in item list
     * @param {Object} data
     */
    function createItem(data){
        items.push({"id": data.id, "data": data});
        let template = $(`.${itemTemplateClass}`).clone();
        let type = data.type ? 'plus' : 'minus';
        let symbol = data.type ? '+' : '-';
        
        template.removeClass(itemTemplateClass).removeClass(ss.hide);
        $(itemList).append(template);
        $(template).animateCss("flipInX");
        template.attr("id", `item-card-${data.id}`);
        template[0].dataset.id = data.id; //TODO: maybe refactor
        template.find("div.title span").html(data.title);
        template.find("div.total span").html(symbol + data.total);
        template.addClass(`item-card--${type}`);
        
        if (!data.new) {
            removeIcons(template);
        }
    };
    
    /**
     * Load item by current date
     */
    function loadItem(){
        let date = getCurrentDate();
        $.ajax({
            url: Routing.generate("api_item_by_date"),
            data: {"date": date},
            success: function(response){
                let data = JSON.parse(response.data);
                clearItems();
                
                if (response.code == 200 && data.length > 0) {
                    setTimeout(function(){
                        $.each(data, function(key, value){
                            createItem(value);
                        });
                    }, 500);
                }
            }
        });
    };
    
    /**
     * Change item status
     * 
     * @param {Number} status
     * @param {Number} id
     */
    function changeStatusItem(status, id){
        $.ajax({
            url: Routing.generate("api_item_change_status"),
            data: {"item": id, "status": status},
            success: function(response){
                if (response.code == 200) {
                    let item = findItemCard(id);
                    let data = JSON.parse(response.data);
                    $(item).animateCss('flipInY');
                    $(item).addClass(`item-card--${data.statusClass}`);
                    $(item).removeClass("item-card--active");
                    removeIcons(item);
                }
            },
            error: function(response){
                console.log(response.reponseText);
            },
        });
    };
    
    /**
     * Delete item
     * 
     * @param {Number} id
     */
    function deleteItem(id){
        $.ajax({
            url: Routing.generate("app_api_item_delete", {"id": id}),
            success: function(response){
                if (response.code == 200) {
                    let item = findItemCard(id);
                    $(item).animateCss('flipOutX');
                    setTimeout(function(){
                        $(item).remove();
                    }, 550);
                }
            },
            error: function(response){
                console.log(response.reponseText);
            },
        });
    };
    
    function bind(){
        $(ss.modalCloseButton).on("click", function(){
            toggleModal($(this).parents(ss.modal));
        });
        
        $(document).ready(loadItem);
        
        $(document).ready(function(){
            $( ".modal-custom" ).draggable({ containment: "body", handle: ".modal-custom__title" });
        });
        
        $(document).on("click", ".icon-action.icon-done", function(){
            let id = $(this).parents("div[id^=item-card-]").data("id");
            changeStatusItem(3, id); // 3 -status close
        });
        
        $(document).on("click", ".icon-action.icon-close", function(){
            let id = $(this).parents("div[id^=item-card-]").data("id");
            changeStatusItem(2, id); // 2 - status cancel
        });
        
        $(document).on("click", ".icon-action.icon-delete", function(){
            let id = $(this).parents("div[id^=item-card-]").data("id");
            deleteItem(id);
        });
        
        itemCreateButton.on("click", function(){
            toggleModal(itemCreateFormModalSelector);
        });
        
        $(document).on("click", "span.close-month", function(){
            calendar.create({ month: $(this).data("month"), year: $(this).data("year")});
        });
        
        $(document).on("click", itemViewModalButtonSelector, function(){
            let id = $(this).parents("div[id^=item-card-]").data("id");
            initItemViewModal(id);
        });
        
        $(document).on("click", cellSelector, function(){
            $(cellSelector).removeClass(cellActiveClass);
            $(this).toggleClass(cellActiveClass);
            updateDateItem();
            loadItem();
        });
        
        $(document).on("submit", itemCreateFormSelector, function(e){
            e.preventDefault();
            let $form = $(this);
            
            $.ajax({
                url: Routing.generate("app_api_item_add"),
                data: $form.serialize(),
                type: "POST",
                success: function(response){
                    $(".errors").remove();
                    
                    switch (response.code) {
                        case 200:
                            let item = JSON.parse(response.data);
                            createItem(item);
                            break;
                        case 500:
                            for (var key in response.data) {
                                let input = $form.find(`[name*="${key}"]`)[0];
                                $(input).before(`<ul class="errors"><li>${response.data[key]}</li></ul>`);
                            }
                            break;
                        default:
                            //TODO: functional for infoming user about invalid request
                            break;
                    }
                },
                error: function(e){
                    console.log(e);
                }
            });
        });
    };
    
    function init(){
        bind();
        calendar.create();
        updateDateItem();
    }
    
    init();
}