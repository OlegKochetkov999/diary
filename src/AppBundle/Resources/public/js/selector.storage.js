class SelectorStorage {
    get hide() {return this._hideClass;}
    get modal() {return this._modal;}
    get modalCloseButton() {return this._modalCloseButton;}
    
    constructor() {
        this._hideClass = "hide";
        this._modal = ".modal-custom";
        this._modalCloseButton = ".button-action.modal-custom-close";
    }
}