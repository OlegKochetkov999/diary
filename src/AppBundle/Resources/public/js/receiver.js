var appReceiver;

$(function(){
    appReceiver = new AppReceiver();
});

function AppReceiver() {

    var that = this;

    /**
     * Ping server on new message and output on display
     * @returns {undefined}
     */
    this.receiver = function(){
        $.ajax({
            url: Routing.generate("app_emergency_message"),
            success: function(data) {
                if (data) {
                    that.openPopup(data);
                }
            },
            complete: function() {
                if (CHECK_MESSAGES_INTERVAL && CHECK_MESSAGES_INTERVAL > 0) {
                    setTimeout(that.closePopup, CHECK_MESSAGES_INTERVAL);
                }
                that.query(Routing.generate("app_emergency_reload-notifications"), '.notifications');
            }
        });
    };

    this.query = function(url, blockSelector, callback){
        $.ajax({
            url: url,
            success: function(data) {
                if (data) {
                    $(blockSelector).html(data);
                }
            },
            complete: function(){
                if (typeof callback !== "undefined" && callback !== null) {
                    callback();
                }
            }
        });
    };

    this.openPopup = function(data){
        $(".popup-message-content").html(data);
        $(".popup-message").show();
        
        if ($("#sound_message").val() == "on") {
            var incomingMessageAudio = new window.Audio($("#audio-message").val());
            incomingMessageAudio.loop = false;
            incomingMessageAudio.play();
        }
        $(".icon--notification").css('fill', 'red');
    };

    this.closePopup = function() {
        $(".popup-message").hide("slow");
        $(document).remove(".popup-message");
    }
    
    this.bind = function(){
        $(document).on("click", ".close-popup-message", function(){
            $.ajax({
                url: Routing.generate("app_notificationmessage_change-status"),
                type: "GET",
                data: {"idMessages": $("#id_messages").val()},
            });
            that.closePopup();
        });
    };
    
    this.init = function(){
        that.bind();
        setInterval(that.receiver, 15000);
    };
    
    that.init();
}
