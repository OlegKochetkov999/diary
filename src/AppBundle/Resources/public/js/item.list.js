let appItemList;
$(function () {
    appItemList = new ItemList();
});

function ItemList(){
    let blockListSelector = "#item-list";
    let itemCardSelector = ".item-card";
    let itemActiveClass = "item-card--active";
    
    function bind(){
        $(blockListSelector).on("click", itemCardSelector, function(){
            $(itemCardSelector).removeClass(itemActiveClass);
            $(this).toggleClass(itemActiveClass);
            //TODO: load about modal
        });
    };
    
    function init(){
        bind();
    }
    
    init();
}


