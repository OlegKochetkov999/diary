<?php

namespace AppBundle;

final class Events
{
    /**
     * For the event naming conventions, see:
     * http://symfony.com/doc/current/components/event_dispatcher.html#naming-conventions.
     *
     * @Event("Symfony\Component\EventDispatcher\GenericEvent")
     *
     * @var string
     */
    const COMMENT_CREATED = 'comment.created';
}
