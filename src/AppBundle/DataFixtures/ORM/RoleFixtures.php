<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User\Role;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class RoleFixtures extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $permissionsAdmin = [
            "ROLE_ITEM_LIST", "ROLE_ITEM_VIEW", "ROLE_ITEM_ADD", "ROLE_ITEM_EDIT", "ROLE_ITEM_DELETE",
            "ROLE_CATEGORY_LIST", "ROLE_CATEGORY_VIEW", "ROLE_CATEGORY_ADD", "ROLE_CATEGORY_EDIT", "ROLE_CATEGORY_DELETE",
            "ROLE_USER_LIST", "ROLE_USER_VIEW", "ROLE_USER_ADD", "ROLE_USER_EDIT", "ROLE_USER_DELETE",
            "ROLE_USER-SETTING_LIST", "ROLE_USER-SETTING_VIEW", "ROLE_USER-SETTING_ADD", "ROLE_USER-SETTING_EDIT", "ROLE_USER-SETTING_DELETE",
        ];
        
        $permissionsUser = [
            "ROLE_ITEM_LIST", "ROLE_ITEM_VIEW", "ROLE_ITEM_ADD", "ROLE_ITEM_EDIT", "ROLE_ITEM_DELETE",
            "ROLE_CATEGORY_LIST", "ROLE_CATEGORY_VIEW", "ROLE_CATEGORY_ADD", "ROLE_CATEGORY_EDIT", "ROLE_CATEGORY_DELETE",
            "ROLE_USER-SETTING_VIEW", "ROLE_USER-SETTING_EDIT",
        ];
        
        $role = new Role();
        $role->setPermissions($permissionsAdmin);
        $role->setCode("ROLE_ADMIN");
        $role->setTitle("Администратор");
        $manager->persist($role);
        
        $role = new Role();
        $role->setPermissions($permissionsUser);
        $role->setCode("ROLE_USER");
        $role->setTitle("Пользователь");
        $manager->persist($role);
        
        $manager->flush();
    }
    
    public function getOrder()
    {
        return 1;
    }
}
