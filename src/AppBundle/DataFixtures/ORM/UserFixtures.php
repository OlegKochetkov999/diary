<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use AppBundle\Entity\User\User;
use AppBundle\Entity\User\Role;

class UserFixtures extends AbstractFixture implements ContainerAwareInterface, OrderedFixtureInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $repository = $manager->getRepository(Role::class);
        $adminRole = $repository->findOneByCode("ROLE_ADMIN");
        $userRole = $repository->findOneByCode("ROLE_USER");
        
        $admin = new User();
        $admin->setUsername('admin007');
        $admin->setEmail('rainmen9999@gmail.com');
        $admin->setRole($adminRole);
        $admin->setPassword(password_hash('909rheujdjhjn101', PASSWORD_BCRYPT));
        $manager->persist($admin);

        $user = new User();
        $user->setUsername('user');
        $user->setEmail('rainmen999@mail.ru');
        $admin->setRole($userRole);
        $user->setPassword(password_hash('909rheujdjhjn101', PASSWORD_BCRYPT));
        $manager->persist($user);

        $manager->flush();
    }
    
    public function getOrder()
    {
        return 2;
    }
}
