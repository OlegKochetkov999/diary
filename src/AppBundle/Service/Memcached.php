<?php
namespace AppBundle\Service;

use Symfony\Component\Cache\Adapter\MemcachedAdapter;

class Memcached
{
    protected $client;

    /**
     * @param string $host
     * @param int $port
     */
    public function __construct($host, $port)
    {
        $port = !empty($port) ? ":$port" : '';
        $this->client = MemcachedAdapter::createConnection("memcached://" . $host . $port);
    }

    /**
     * @param string $key
     * @return array
     */
    public function get($key = null)
    {
        return $this->client->get($key);  
    }
    
    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public function set($key = null, $value)
    {
        return $this->client->set($key, $value);  
    }
}
