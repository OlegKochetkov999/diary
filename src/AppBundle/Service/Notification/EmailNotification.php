<?php
namespace AppBundle\Service\Notification;

use AppBundle\Entity\Setting;
use AppBundle\Service\SettingService;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\User\User;

class EmailNotification
{
    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var string
     */
    protected $mailerHost;

    /**
     * @var string
     */
    protected $mailerUser;

    public function __construct(SettingService $settingService)
    {
        $setting = $settingService->get();
        $this->mailerHost = $setting->getMailerHost();
        $this->mailerUser = $setting->getMailerUser();

        //$transport = \Swift_SmtpTransport::newInstance($this->mailerHost, 587, 'tls') - for sending emails on local computer
        $transport = \Swift_SmtpTransport::newInstance($this->mailerHost, 25)
            ->setUsername($this->mailerUser)
            ->setPassword($setting->getMailerPassword());
        $this->mailer = \Swift_Mailer::newInstance($transport);
    }

    /**
     * @param string $email
     * @param string $notification
     * @return bool
     */
    public function send($email, $notification, $sender = null)
    {
        if ($this->mailerUser && $this->mailerHost) {
            $mailerUser = $this->mailerUser;
            //If mailer user doesn't contains mail letter fill it with host domain
            if (strpos($mailerUser, '@') === false) {
                $hostParts = explode('.', $this->mailerHost);
                $mailerUser .= '@' . implode('.', array_slice($hostParts, -2, 2));
            }
            $subject = "Уведомление". ($sender ? ' от '.$sender : '');
            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($mailerUser)
                ->setTo($email)
                ->setBody($notification);
            try {
                $this->mailer->send($message);
                return true;
            } catch (\Swift_TransportException $e) {
            }
        }

        return false;
    }
}
