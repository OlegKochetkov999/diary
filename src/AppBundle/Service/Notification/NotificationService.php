<?php
namespace AppBundle\Service\Notification;

use AppBundle\Entity\Notice;
use AppBundle\Entity\Item\Item;
use AppBundle\Entity\User\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NotificationService
{
    /** @var ContainerInterface */
    private $container;
    
    /** @var User */
    private $user;
    
    /**
     * NotificationService constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        //->get('security.token_storage')->getToken()->getUser();
    }

    /**
     * @param Notice $notice
     * @param Item $item
     */
    public function send(Notice $notice, Item $item)
    {
        if ($template = $notice->getNotificationTemplate()) {
            $message = $this->generateMessage($template, $emergency);
            
            foreach ($template->getTypes() as $type) {
                if ($this->container->has("app.notification.$type")) {
                    $this->container->get("app.notification.$type")->send($user, $message, $item);
                }
            }
        }
    }

    /**
     * @param NotificationTemplate $template
     * @param Emergency $emergency
     */
    private function generateMessage(NotificationTemplate $template, Item $item)
    {
        $message = $template->getDescription() . PHP_EOL;
            
        foreach ($template->getFields() as $field) {
            $title = NotificationTemplate::$fieldList[$field];
            if (property_exists(Emergency::class, $field)) {
                $methodName = "get" . ucfirst($field);
                
                if (method_exists(Emergency::class, $methodName)) {
                    $value = $emergency->$methodName();
                 
                    if ($value instanceof \DateTime) {
                        $value = $value->format("d.m.Y H:i:s");
                    }
                   
                    $message .= "$title: $value" . PHP_EOL;
                }
            }
        }
            
        return $message;
    }
}
