<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserAttributeRepository extends EntityRepository
{
    public function getAttributeQueryBuilder($user)
    {
        return $this->createQueryBuilder("a")->where("a.user = $user");
    }
}
