<?php

namespace AppBundle\Entity\Item;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\UserAttribute;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemGroupRepository")
 * @ORM\Table(name="item_groups")
 */
class Group extends UserAttribute
{
}
