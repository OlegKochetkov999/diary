<?php

namespace AppBundle\Entity\Item;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\UserAttribute;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemRepository")
 * @ORM\Table(name="items")
 */
class Item extends UserAttribute
{
    const TYPE_CONSUMPTION = false;
    const TYPE_PROCEEDS = true;
    
    public static $types = [
        self::TYPE_CONSUMPTION => 'Расход',
        self::TYPE_PROCEEDS => 'Доход'
    ];
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;
    
    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", nullable=true)
     */
    private $total;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;
    
    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $type;
    
    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName = "id")
     */
    private $category;
    
    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="Group")
     * @ORM\JoinColumn(name="group_id", referencedColumnName = "id")
     */
    private $group;
    
    public function __construct($user = null)
    {
        parent::__construct($user);
    }
    
    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     * @return Item
     */
    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }
    
    /**
     * @return bool
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param boolean $type
     * @return Item
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    
    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return Item
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }
    
    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Item
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }
    
    /**
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }
    
    /**
     * @param Group $group
     * @return Item
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }
    
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Item
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    
    public function __toString() {
        return $this->title;
    }
}
