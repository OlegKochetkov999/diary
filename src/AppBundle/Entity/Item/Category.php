<?php

namespace AppBundle\Entity\Item;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\UserAttribute;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ItemCategoryRepository")
 * @ORM\Table(name="item_categories")
 */
class Category extends UserAttribute
{
}
