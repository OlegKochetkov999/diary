<?php

namespace AppBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @UniqueEntity("email", message="Указанный адрес электронной почты уже зарегистрирован")
 * @UniqueEntity("username", message="Указанный логин уже зарегистрирован")
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "ФИО не должно быть длиннее {{ limit }} символов"
     * )
     */
    private $fullname;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, unique=true)
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "Логин не должен быть длиннее {{ limit }} символов"
     * )
     * @Assert\NotBlank(message="Логин не должен быть пустым")
     */
    private $username;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateOfBirth;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateOfRegistration;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @Assert\Email(
     *     message = "Адрес электронной почты '{{ value }}' имеет не верный формат"
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "Пароль не должен быть длиннее {{ limit }} символов"
     * )
     * @Assert\NotBlank(message="Пароль не должен быть пустым")
     */
    private $password;

    /**
     * @var UserSetting
     *
     * @ORM\OneToOne(targetEntity="UserSetting", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="setting_id", referencedColumnName = "id")
     */
    private $setting;
    
    /**
     * @var Role
     * 
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName = "id")
     */
    private $role;
    
    public function __construct()
    {
        $this->setting = new UserSetting();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isAdmin()
    {
        return ($this->role and $this->role->isAdmin());
    }
    
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * @param string $name
     */
    public function setFullname(string $name)
    {
        $this->fullname = $name;
    }
    
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param \DateTime $date
     */
    public function setDateOfBirth(\DateTime $date)
    {
        $this->dateOfBirth = $date;
    }
    
    public function getDateOfRegistration()
    {
        return $this->dateOfRegistration;
    }

    /**
     * @param \DateTime $date
     */
    public function setDateOfRegistration(\DateTime$date)
    {
        $this->dateOfRegistration = $date;
    }
    
    public function getSetting()
    {
        return $this->setting;
    }

    /**
     * @param UserSetting $setting
     */
    public function setSetting(UserSetting $setting)
    {
        $this->setting = $setting;
    }
    
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * Returns the role with permissions to the user for security.
     */
    public function getRole()
    {
        return $this->role;
    }

    public function setRole(Role $role)
    {
        $this->role = $role;
    }
    
    public function getRoles()
    {
        return [$this->role->getCode()];
    }
    
    public function getSalt(){}

    public function eraseCredentials(){}
}
