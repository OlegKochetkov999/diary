<?php

namespace AppBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\User\Permission;

/**
 * @ORM\Table(name="roles")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoleRepository")
 * @UniqueEntity(fields = {"title"}, errorPath = "title", message = "Роль с таким наименованием уже имеется в системе")
 */
class Role
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", nullable = true)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     * @Assert\NotBlank(message="Наименование роли не может быть пустым")
     */
    protected $title;

     /**
     * @var string
     *
     * @ORM\Column(type = "array", nullable = true)
     */
    protected $permissions;

    public function __construct()
    {
        $this->permissions = [];
        $this->role = "ROLE_DEFAULT";
    }
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * {@inheritdoc}
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

     /**
     * @param string $permission
     */
    public function addPermission(string $permission)
    {
        if (!$this->hasPermission($permission)) {
            $this->permissions[] = strtoupper($permission);
        }
    }

    /**
     * @param string $permission
     *
     * @return boolean
     */
    public function hasPermission(string $permission): bool
    {
        return in_array(strtoupper($permission), $this->permissions, true);
    }

    /**
     * @return array
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @return array
     */
    public function getPermissionsString(): array
    {
        return Permission::getPermissions($this->permissions);
    }
    
    /**
     * @param array $permissions
     */
    public function setPermissions(array $permissions)
    {
        $this->permissions = $permissions;
    }
    
    public function isAdmin()
    {
        return ($this->code and $this->code == "ROLE_ADMIN");
    }
    
    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->title;
    }
}
