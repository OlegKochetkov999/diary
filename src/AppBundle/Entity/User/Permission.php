<?php

namespace AppBundle\Entity\User;

class Permission
{
    public static $permissions = [
        "Статьи" => "/item/",
        "Категории" => "/category/",
        "Пользователи" => "/user/",
        "Настройки пользователя" => "/user-setting/"
    ];

    public static $regulation = [
        "LIST" => "Просмотр списка", 
        "VIEW" => "Подробный просмотр", 
        "ADD" => "Добавление", 
        "EDIT" => "Редактирование", 
        "DELETE" => "Удаление"
    ];

    public static function getRoles(): array
    {
        $roles = [];
        foreach (self::$permissions as $key => $value) {
            $roles[$key] = [];
            foreach (self::$regulation as $regulation => $title) {
                $roles[$key][$title] = "ROLE" . str_replace("/", "_", strtoupper($value)) . $regulation;
            }
        }
        return $roles;
    }

    /**
     * Get array permission by array codes
     * @param array $codes
     * @return array
     */
    public static function getPermissions($codes): array
    {
        $permissions = [];
        foreach ($codes as $code) {
            $arrayCode = explode("_", $code);
            if (array_key_exists(1, $arrayCode) and
                array_key_exists(2, $arrayCode) and
                array_key_exists($arrayCode[2], self::$regulation)) {
                $action = self::$regulation[trim($arrayCode[2])];
                $catalog = array_search('/' . strtolower($arrayCode[1]) . '/', self::$permissions);
                $permissions[] = "$action $catalog";
            }
        }
        return $permissions;
    }
}
