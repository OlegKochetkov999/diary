<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\User\User;

/**
 * @MappedSuperclass
 */
class UserAttribute
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "Наименование не должно быть длиннее {{ limit }} символов"
     * )
     * @Assert\NotBlank(message="Наименование не должно быть пустым")
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="removed_at", type="datetime", nullable=true)
     */
    private $removedAt;
    
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName = "id")
     */
    private $user;

    public function __construct($user = null)
    {
        
        $this->createdAt = new \DateTime();
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $date
     */
    public function setCreatedAt(\DateTime $date)
    {
        $this->createdAt = $date;
        return $this;
    }
    
    public function getRemovedAt()
    {
        return $this->removedAt;
    }

    /**
     * @param \DateTime $date
     */
    public function setRemovedAt(\DateTime $date)
    {
        $this->removedAt = $date;
        return $this;
    }
    
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }
    
    /**
     * @param User $user
     * @return boolean
     */
    public function isOwner($user)
    {
        return (!$this->user or $this->user == $user);
    }
    
    public function __toString() 
    {
        return $this->title;
    }
}
