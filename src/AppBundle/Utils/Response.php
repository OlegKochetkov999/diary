<?php

namespace AppBundle\Utils;

class Response
{
    public static $responses = [
        200 => ["code" => 200, "data" => "OK"],
        400 => ["code" => 400, "data" => "Неверный запрос"],
        401 => ["code" => 401, "data" => "Пользователь не авторизован в системе"],
        403 => ["code" => 403, "data" => "Ограничен доступ"],
        500 => ["code" => 500, "data" => "Ошибка на сервере"]
    ];
    
    /**
     * @param int $code
     * @param String|Array $data
     * 
     * @return array
     */
    public static function generate($code, $data = null)
    {
        if (array_key_exists($code, self::$responses)) {
            $response = self::$responses[$code];
            if ($data) {
                $response["data"] = $data;
            }
            
            return $response;
        }
        
        return self::$responses[500];
    }
}
